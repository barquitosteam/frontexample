
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    karma = require('gulp-karma');

var ruta = 'app/';

gulp.task('build', function() {
    gulp.src([
      ruta + '**/*.js'
    ])
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('build-dev', function() {
        gulp.src([
      ruta + '**/*.js'
    ])
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('./dist/'));
});

/**   Tests    **/

gulp.task('test', function() {
  return gulp.src([])
    .pipe(karma({
      configFile: 'tests/karma.conf.js',
      action: 'run'
    }));
});
